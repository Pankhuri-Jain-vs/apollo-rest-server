const { RESTDataSource } = require("apollo-datasource-rest");

class BooksAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "";
  }

  async getCurrentBTCRates() {
    const btcData = await this.get("https://api.coindesk.com/v1/bpi/currentprice.json");
    return btcData;
  }

  async getYourAge(name) {
    const ageData = await this.get(`https://api.agify.io/?name=${name}`);
    return ageData;
  }
}

module.exports = BooksAPI;