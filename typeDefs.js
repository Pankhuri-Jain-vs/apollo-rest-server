const { gql } = require("apollo-server-express");

const typeDefs = gql`
  type User {
    name: String!
    email: String!
    id: Int!
  }
  type Coins{
    code: String!
    description: String!
    rate: String!
    rate_float: Float!
    symbol: String!
  }
  type Currency{
    name: [Coins]!
  }
  type Btc{
    chartName: String!
    disclaimer: String!
  }
  type UserAge{
    name: String!
    age: Int!
    count: Int!
  }
  type Query {
    getAllUsers: [User!]!
    getCurrentBTCRates : Btc!
    getYourAge( name : String! ) : [UserAge]!
  }
`;

module.exports = typeDefs;

// type Joke {
//     id: Int!
//     content: String!
//     ratings: [Rating]
//   }
//   type Rating {
//     id: Int!
//     jokeId: Int!
//     score: Int!
//   }
//   type Query {
//     joke(id: Int!): Joke
//     jokes: [Joke]
//     rating(id: Int!): Rating
//     ratings: [Rating]
//   }
//   type Mutation {
//     rating(jokeId: Int!, score: Int!): Rating
//   }
