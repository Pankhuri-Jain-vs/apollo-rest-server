const users = require("./data");
const resolvers = {
  Query: {
    // getAllUsers() {
    //   return users;
    // },
    getAllUsers: async (_source, _args, { dataSources }) => {
      const result = await dataSources.userApi.getUsers();
      return result;
    },
    getCurrentBTCRates: async (_source, _args, { dataSources }) => {
      const result = await dataSources.booksApi.getCurrentBTCRates();
      console.log('result', result);
      return result;
    },
    getYourAge: async (_source, { name }, { dataSources }) => {
      const result = await dataSources.booksApi.getYourAge(name);
      return result;
    },
    //   joke: async (_source, { id }, { dataSources }) =>
    //     dataSources.jokesAPI.getJoke(id),
    //   jokes: async (_source, _args, { dataSources }) =>
    //     dataSources.jokesAPI.getJokes(),
    //   rating: async (_source, { id }, { dataSources }) =>
    //     dataSources.ratingsAPI.getRating(id),
    //   ratings: async (_source, _args, { dataSources }) =>
    //     dataSources.ratingsAPI.getRatings(),
  },
  // Mutation: {
  //   rating: async (_source, { jokeId, score }, { dataSources }) => {
  //     const rating = await dataSources.ratingsAPI.postRating({ jokeId, score })
  //     return rating
  //   },
  // },
};

module.exports = resolvers;
